package com.example.springboothelloworldscheduled.tasks;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class HelloWorldTask {

    @Scheduled(cron = "0 */5 * ? * *")
    public void reportCurrentTime() {
        System.out.println("Hello World!");
    }
}
